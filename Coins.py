"""
Задание:
Есть монеты n различных номиналов.  Можно ли набрать нужную сумму s с помощью этих монет?
Если да, то какое минимальное количество монет k для этого необходимо?  Выведите эти монеты
Например:
    номиналы: 1, 3, 5
    необходимая сумма: 11
    ответ: k = 3: 5, 3, 3 или 5, 5, 1
"""

import itertools


def find_nominals(nominals: list, sum_: int) -> list:
    """ Функция, возвращающая все возможные комбинации монет, необходимых для того, чтобы собрать нужную сумму

    В частном случае, когда номиналы известны заранее, например, nominals = [5, 3, 1],
    решение будет будет выглядеть следующим образом:

    for i in range(sum_ // nominals[0], -1, -1):
        for j in range(sum_ // nominals[1], -1, -1):
            for k in range(sum_ // nominals[2], -1, -1):
                if 5*i + 3*j + 1*k == sum_:
                    coins.append((i, j, k))

    Когда же номиналы и их количество заранее неизвестны, сперва создается список диапазонов ranges,
    из которых затем при помощи метода product модуля itertools создаются всевозможные комбинации i, j, k, ...

    :param list nominals: номиналы монет
    :param int sum_: сумма, которую нужно набрать с помощью монет заданных номиналов nominals
    :return: список кортежей вида [(2, 0, 1), ...], где числа означают количество монет,
        соответствующие (по позициям) номиналам из списка nominals ([5, 3, 1]),
        то есть 2 монеты номиналом 5, 0 монет номиналом 3, 1 монета номиналом 1 и т.д.
    """

    if sum_ == 0:
        return []

    ranges = [range(sum_ // nominal, -1, -1) for nominal in nominals]
    coins = []

    for i in itertools.product(*ranges):
        current_sum = 0

        for j in zip(i, nominals):
            current_sum += j[0]*j[1]

        if current_sum == sum_:
            coins.append(i)

    if coins:
        min_coins_count = min(sum(x) for x in coins)  # минимальное количество монет

    return list(filter(lambda x: sum(x) == min_coins_count, coins))


if __name__ == "__main__":
    nominals = [1, 3, 5]
    wanted_sum = 11

    nominals.sort(reverse=True)
    min_coins_counts = find_nominals(nominals, wanted_sum)

    if min_coins_counts:
        print(f"Чтоб набрать сумму равную {wanted_sum}, "
              f"нашлось {len(min_coins_counts)} комбинаций с наименьшим количеством монет "
              f"равным {sum(min_coins_counts[0])}:\n")
        for combination_no, combination in enumerate(min_coins_counts, 1):
            print(f"- комбинация #{combination_no}:")
            for index, count in enumerate(combination):
                if count:
                    print(f"{count} монет номиналом {nominals[index]}")
            print()
    else:
        print(f"Невозможно набрать сумму равную {wanted_sum} "
              f"с помощью монет, номинал которых {', '.join(map(str, nominals))}")
