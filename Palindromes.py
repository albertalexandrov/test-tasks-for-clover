"""
Задание:
Необходимо найти самую длинную подстроку-палиндром.  Например:
    арозаупаланалапуазора -> арозаупаланалапуазора
    покос -> око
"""


def palindrome(word: str) -> str:
  """ Возвращает самый длинный палиндром в переданной строке """

  start = 0  # позиция, с которой начинается самый длинный палиндром

  for i in range(len(word) // 2):
    if word[i] != word[-i-1]:
      start = i + 1

  stop = None if start == 0 else -start

  return word[start:stop]


assert palindrome('покос') == 'око'
assert palindrome('поккос') == 'окко'
assert palindrome('арозаупаланалапуазора') == 'арозаупаланалапуазора'

print('Все тесты пройдены успешно.')